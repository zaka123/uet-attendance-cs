<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('father_name',50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('cnic', 15)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('emergency_1', 20)->nullable();
            $table->string('emergency_2', 20)->nullable();
            $table->string('address', 100)->nullable();
            $table->foreignId('room_id')->nullable()->constrained()->onDelete('set null');
            $table->string('status', 10)->default('Active');
            $table->date('joining_date')->nullable();
            $table->date('leaving_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('students');
    }
};
