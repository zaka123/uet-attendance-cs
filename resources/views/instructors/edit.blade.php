@extends('layouts.app', ['activePage' => 'teachers', 'pageTitle' => __('New Teacher')])
@section('css')
    <style>
        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        input,
        select {
            background-color: #F3F5F7 !important;
        }

        select option {
            padding: 10px;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <div class="main-box bg-white">
            <div class="row invoiceList mt-2">
                <div class="col-md-6">
                    <h1 class="fw-bold" style="font-family: Arial, Helvetica, sans-serif">{{ __("Edit Instructor's Data") }}</h1>
                </div>
                <div class="col-md-6 text-end mb-3">
                    <a href="{{ route('instructors.index') }}" class="btn btn-secondary">
                        Back
                    </a>
                </div>
                <section class="section dashboard mt-4">
                    <form action="{{ route('instructors.update', $instructor->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row inputs">
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="name">Instructor Name</legend>
                                <input type="text" id="name" name="name" class="name" placeholder="Name"
                                    value="{{ old('name') ?? $instructor->name }}">
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="email">Email</legend>
                                <input type="text" id="email" name="email" class="name"
                                    placeholder="example@mail.com" value="{{ old('email') ?? $instructor->email }}">
                            </div>
                        </div>
                        <div class="row inputs">
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="password">Password</legend>
                                <input type="text" class="name" name="password">
                            </div>
                        </div>
                        <div class="buttons_group mt-4 mb-5">
                            <div class="buttons_group1">
                                <button type="submit" class="generate">Update</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script></script>
@endsection
