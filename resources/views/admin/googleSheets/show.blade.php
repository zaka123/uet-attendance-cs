@extends('layouts.app', ['activePage' => 'googleSheets', 'pageTitle' => __('Google Sheet Details')])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <style>
        .plus-icon {
            padding: 4px 7px;
            /* background-color: #17c914; */
            background-color: #198754;
            border-radius: 9px;
            margin-right: 5px !important;
            padding-bottom: 6px !important;
            color: white;
        }

        .pagination {
            width: fit-content;
            float: right;
        }

        .pagination>li>a {
            background-color: white;
            color: #5A4181;
            font-weight: 600;
            margin-left: 12px !important;
            box-shadow: none !important;
            width: 38px !important;
            height: 35px !important;
            border-radius: 0px !important;
        }

        .pagination>li>a:focus,
        .pagination>li>a:hover,
        .pagination>li>span:focus,
        .pagination>li>span:hover {
            color: black;
            border-color: white;
        }

        .pagination>.active>a {
            color: white;
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A !important;
        }

        .pagination>.active>a:hover {
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A;
        }

        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <div class="main-box bg-white mb-5">
            <div class="row invoiceList mt-2">
                <div class="col-md-6">
                    <h2>{{ $googleSheet->course_name }} - {{ $googleSheet->month }}</h2>
                </div>
                <div class="col-md-6 text-end mb-3">
                    <a href="{{ route('sheets.index') }}" class="btn btn-secondary">
                        Back
                    </a>
                </div>
                {{-- <div class="d-flex col-md-6 justify-content-end mb-3 gap-2 position-relative">
                    <a href="{{ route('sheets.edit', $googleSheet->id) }}" class="btn btn-primary">
                        Edit
                    </a>
                    <a href="javascript:void(0)" type="button" data-bs-toggle="modal" data-bs-target="#deleteSheetModal" class="btn btn-danger">
                        Delete
                    </a>
                </div> --}}
                <div class="col-md-12">
                    <div style="overflow:auto">
                        <iframe
                            src="{{ $googleSheet->sheet_link }}"
                            frameborder="0" width="100%" height="800"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('modals')
        {{-- delete sheet confirmation modal --}}
        <div class="modal fade" id="deleteSheetModal" tabindex="-1"
            aria-labelledby="deleteSheetModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteSheetModalLabel">Warning!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {{-- form for deleting sheet --}}
                    <form id="deleteSheetForm" action="{{ route('sheets.destroy', $googleSheet->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-body">
                            Are you sure you want to delete this sheet?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger">Yes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

