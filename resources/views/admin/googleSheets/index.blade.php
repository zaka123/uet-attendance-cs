@extends('layouts.app', ['activePage' => 'googleSheets', 'pageTitle' => __('Google Sheets')])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <style>
        .plus-icon {
            padding: 4px 7px;
            /* background-color: #17c914; */
            background-color: #198754;
            border-radius: 9px;
            margin-right: 5px !important;
            padding-bottom: 6px !important;
            color: white;
        }

        .pagination {
            width: fit-content;
            float: right;
        }

        .pagination>li>a {
            background-color: white;
            color: #5A4181;
            font-weight: 600;
            margin-left: 12px !important;
            box-shadow: none !important;
            width: 38px !important;
            height: 35px !important;
            border-radius: 0px !important;
        }

        .pagination>li>a:focus,
        .pagination>li>a:hover,
        .pagination>li>span:focus,
        .pagination>li>span:hover {
            color: black;
            border-color: white;
        }

        .pagination>.active>a {
            color: white;
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A !important;
        }

        .pagination>.active>a:hover {
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A;
        }

        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <div class="main-box bg-white mb-5">
            <div class="row invoiceList mt-2">
                <div class="col-md-6 mb-2">
                    <h2>{{ __('Google Sheets List') }}</h2>
                </div>
                <div class="d-flex col-md-6 justify-content-end mb-3 gap-2 position-relative">
                    {{-- <input type="text" name="search" class="name border w-50" placeholder="Search"> --}}
                    {{-- <i class="bi bi-search text-secondary position-absolute top-50 translate-middle-y" style="right:200px"></i> --}}

                    {{-- <a href="{{ route('user.sheets.create') }}" class="btn text-white" style="background-color:#D1884B">
                        Add Google Sheet
                    </a> --}}
                </div>
                <div class="table-responsive">
                    <table class="table invoiceTable table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Course</th>
                                <th>Month</th>
                                <th>Instructor</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($googleSheets as $sheet)
                            <tr>
                                <td style="5%">{{ $loop->iteration }}</td>
                                <td style="width: 20%">{{ $sheet->course_name }}</td>
                                <td style="width: 15%">{{ $sheet->month }}</td>
                                <td style="width: 30%;word-break:break-word">
                                    {{ $sheet->user->name }}
                                </td>
                                <td>
                                    {{-- <a href="{{ route('user.sheets.edit', $sheet->id) }}" title="Edit">
                                        <span class="edit"><i class="bi bi-pencil-square"></i></span>
                                    </a> --}}
                                    <a href="{{ route('sheets.show', $sheet->id) }}"  class="btn btn-sm btn-primary rounded">View Sheet</a>
                                    {{-- <button class="border-0" data-bs-toggle="modal" data-bs-target="#deleteGuestModal{{ $sheet->id }}"> <span class="delete"><i
                                                class="bi bi-trash"></i></span>
                                    </button> --}}
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No record found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {{-- Adding custom pagination --}}
                {{ $googleSheets->links('pagination::custom') }}
            </div>
        </div>
    </main>
@endsection
@section('modals')
    @foreach ($googleSheets as $sheet)
        {{-- delete sheet confirmation modal --}}
        <div class="modal fade" id="deleteGuestModal{{ $sheet->id }}" tabindex="-1"
            aria-labelledby="deleteGuestModal{{ $sheet->id }}Label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteGuestModal{{ $sheet->id }}Label">Warning!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {{-- form for deleting sheet --}}
                    <form id="deleteGuestForm" action="{{ route('sheets.destroy', $sheet->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-body">
                            Are you sure you want to delete this record?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger">Yes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
