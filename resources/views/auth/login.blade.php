<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UET CS & IT ATTENDENCE - Login</title>
    <!--Bootstrap CSS Files -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css"
        integrity="sha384-b6lVK+yci+bfDmaY1u0zE8YYJt0TZxLEAFyYSLHId4xoVvsrQu3INevFKo+Xir8e" crossorigin="anonymous">
    <!--Main CSS File -->
    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="hero">
            {{-- Logo --}}

            <div class="login-image">
                <div class="dynamic_logo--wrapper  position-relative">
                    <img src="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}" class="dynamic_logo--dynamic"
                        id="image">
                    <img src="{{ asset('assets/img/UET-ASSETS/uet-logo-no-bg-updated.png') }}"
                        class="dynamic_logo--static">
                </div>
            </div>

            {{-- Login Form --}}
            <div class="credentails">
                <form method="POST" action="{{ route('login') }}" class="">
                    @csrf
                    <div class="loginInfo">
                        <h1 class="mb-3 login-heading">Login</h1>
                        <div class="mb-2 email  d-flex align-items-center flex-column">
                            <legend>Email Address</legend>
                            <input id="email" type="email" name="email" placeholder="example@gmail.com"
                                {{-- readonly onfocus="this.removeAttribute('readonly')" --}}>
                            <i class="bi bi-envelope"></i>
                            @error('email')
                                <span class="text-danger d-inline-block">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>

                        <div class="mb-2 password d-flex align-items-center flex-column">
                            <legend>Password</legend>
                            <input type="password" id="password" name="password" placeholder="Password"
                                {{-- readonly onfocus="this.removeAttribute('readonly')" --}}>
                            <i class="bi bi-lock"></i>
                            @error('password')
                                <span class="text-danger d-inline-block">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>

                        <div class="selection mb-3 mt-3">
                            <input type="checkbox" id="remember" class="">
                            <label for="remember" class="">Remember me</label>
                        </div>
                        <div class="login">
                            <button type="submit">
                                <i class="bi bi-lock-fill"></i>
                                {{ __('LOGIN') }}</button>
                        </div>
                        {{-- @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('assets/Bootstrap/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script>
        var image = document.getElementById('image');
        var rotation = 0;
        window.addEventListener('load', function() {
            setInterval(function() {
                rotation += 5;
                image.style.transform = 'rotate(' + rotation + 'deg)';
                image.style.transition = 'all 0.3s ease';
            }, 100);
        });
    </script>
</body>

</html>
