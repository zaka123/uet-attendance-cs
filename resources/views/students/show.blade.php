@extends('layouts.app', ['activePage' => 'teachers', 'pageTitle' => __('Teacher Details')])

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <style>
        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        {{-- Guest Details --}}
        <div class="main-box bg-white">
            <div class="row invoiceList mt-2">
                <div class="row mb-3 d-flex align-items-center pe-0">
                    <div class="col-md-6">
                        <h2>{{ __('Teacher Details') }}</h2>
                    </div>
                    <div class="col-md-6 text-end pe-0">
                        <a href="{{ route('students.index') }}" class="btn btn-secondary">
                            Back
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">

                        <tbody>
                            <tr>
                                <td class="text-start"><strong>{{ __('Name') }}</strong></td>
                                <td class="text-start">{{ $student->name }}</td>
                                <td class="text-start"><strong>{{ __('Father Name') }}</strong></td>
                                <td class="text-start">{{ $student->father_name }}</td>
                                <td class="text-start"><strong>{{ __('Phone') }}</strong></td>
                                <td class="text-start">{{ $student->phone }}</td>
                            </tr>
                            <tr>
                                <td class="text-start"><strong>{{ __('Email') }}</strong></td>
                                <td class="text-start">{{ $student->email }}</td>
                                <td class="text-start"><strong>{{ __('Department') }}</strong></td>
                                <td class="text-start">{{ $student->room->room_code }}</td>
                                <td class="text-start"><strong>{{ __('Address') }}</strong></td>
                                <td class="text-start">{{ $student->address }}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- Guest Payment Details --}}
        <div class="main-box bg-white mt-3">
            <div class="row invoiceList mt-2">
                <div class="row mb-3 d-flex align-items-center pe-0">
                    <div class="col-md-6">
                        <h2>{{ __('Attendance Details') }}</h2>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table invoiceTable table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Attendance Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($student->invoices as $invoice)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ \Carbon\Carbon::parse($invoice->invoice_date)->format('d M Y') }}</td>
                                    <td>
                                        <button class="{{ $invoice->status === 'Received' ? 'present' : 'absent' }}">
                                            {{ $invoice->status === 'Received' ? 'Present' : 'Absent' }}
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No Record Found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
@endsection
