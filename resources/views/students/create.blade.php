@extends('layouts.app', ['activePage' => 'teachers', 'pageTitle' => __('New Teacher')])
@section('css')
    <style>
        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        input,
        select {
            background-color: #F3F5F7 !important;
        }

        select option {
            padding: 10px;
        }
    </style>
@endsection
@section('content')
    {{-- <main id="main" class="main">
        <div class="pagetitle pb-4 ">
            <!-- check for validation errors -->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                <h1>{{ __('New Teacher') }}</h1>
            </div>
            <div class="col-md-6 text-end mb-3">
                <a href="{{ route('students.index') }}" class="btn btn-secondary">
                    Back
                </a>
            </div>
        </div>
        <section class="section dashboard mt-4">
            <form action="{{ route('students.store') }}" method="post">
                @csrf

                <div class="row inputs">
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="name">Teacher Name</legend>
                        <input type="text" id="name" name="name" class="name" placeholder="Name"
                            value="{{ old('name') }}">
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="father_name">Father Name</legend>
                        <input type="text" id="f_name" name="father_name" class="name" placeholder="Father Name"
                            value="{{ old('father_name') }}">
                    </div>
                </div>
                <div class="row inputs">
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="email">Email</legend>
                        <input type="email" id="email" name="email" class="name" placeholder="example@mail.com"
                            value="{{ old('email') }}">
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="phone">Contact</legend>
                        <input type="text" id="phone" name="phone" class="name" placeholder="03123456789"
                            value="{{ old('phone') }}">
                    </div>
                </div>
                <div class="row inputs">
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="cnic">CNIC Number</legend>
                        <input type="number" id="cnic" name="cnic" class="name" placeholder="2201234567895"
                            value="{{ old('cnic') }}">
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="address">Address</legend>
                        <input type="text" id="address" name="address" class="name" placeholder="University Road"
                            value="{{ old('address') }}">
                    </div>
                </div>
                <div class="row inputs">
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="room_id">Department</legend>
                        <select name="room_id" id="" class="name">
                            <option value="">Select...</option>
                            @php
                                $rooms = \App\Models\Room::where('status', 'Available')->get();
                            @endphp
                            @foreach ($rooms as $room)
                                <option value="{{ $room->id }}">{{ $room->room_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="joining_date">Joining Date</legend>
                        <input type="date" id="date" name="joining_date" class="name"
                            value="{{ now()->format('Y-m-d') }}">
                    </div>
                </div>
                <div class="row inputs">
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="leaving_date">Leaving Date</legend>
                        <input type="date" id="leaving_date" name="leaving_date" class="name"
                            value="{{ old('leaving_date') }}">
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <legend for="status">Status</legend>
                        <select name="status" id="" class="name">
                            <option value="Active" selected>Active</option>
                            <option value="Left" @if (old('status') == 'Left') selected @endif>Left</option>
                        </select>
                    </div>
                </div>
                <div class="buttons_group mt-4 mb-5">
                    <div class="buttons_group1">
                        <button type="submit" class="generate">Save</button>
                    </div>
                </div>
            </form>
        </section>

    </main> --}}
    <main id="main">
        <div class="main-box bg-white">
            <div class="row invoiceList mt-2">
                <div class="col-md-6">
                    <h1>{{ __('New Instructor') }}</h1>
                </div>
                <div class="col-md-6 text-end mb-3">
                    <a href="{{ route('students.index') }}" class="btn btn-secondary">
                        Back
                    </a>
                </div>
                <section class="section dashboard mt-4">
                    <form action="#" method="post">
                        @csrf
                        <div class="row inputs">
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="name">Instructor Name</legend>
                                <input type="text" id="name" name="name" class="name" placeholder="Name"
                                    value="{{ old('name') }}">
                            </div>
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="email">Email</legend>
                                <input type="email" id="email" name="email" class="name"
                                    placeholder="example@mail.com" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row inputs">
                            <div class="col-12 col-md-6 mb-3">
                                <legend for="name">Password</legend>
                                <input type="password" class="name">
                            </div>
                        </div>
                        <div class="buttons_group mt-4 mb-5">
                            <div class="buttons_group1">
                                <button type="submit" class="generate">Update</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script></script>
@endsection
