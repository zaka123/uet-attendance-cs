@extends('layouts.app', ['activePage' => 'teachers', 'pageTitle' => __('Teachers')])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <style>
        .plus-icon {
            padding: 4px 7px;
            /* background-color: #17c914; */
            background-color: #198754;
            border-radius: 9px;
            margin-right: 5px !important;
            padding-bottom: 6px !important;
            color: white;
        }

        .pagination {
            width: fit-content;
            float: right;
        }

        .pagination>li>a {
            background-color: white;
            color: #5A4181;
            font-weight: 600;
            margin-left: 12px !important;
            box-shadow: none !important;
            width: 38px !important;
            height: 35px !important;
            border-radius: 0px !important;
        }

        .pagination>li>a:focus,
        .pagination>li>a:hover,
        .pagination>li>span:focus,
        .pagination>li>span:hover {
            color: black;
            border-color: white;
        }

        .pagination>.active>a {
            color: white;
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A !important;
        }

        .pagination>.active>a:hover {
            background-color: #BB1A1A !important;
            border: solid 1px #BB1A1A;
        }

        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <div class="main-box bg-white">
            <div class="row invoiceList mt-2">
                <div class="col-md-6">
                    <h2>{{ __('Instructors List') }}</h2>
                </div>
                <div class="d-flex col-md-6 justify-content-end mb-3 gap-2 position-relative">
                    <a href="{{ route('students.create') }}" class="btn text-white" style="background-color:#D1884B">
                        New Instructor
                    </a>
                </div>
                <div class="table-responsive">
                    <table class="table invoiceTable table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Instructor Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($students as $student)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->father_name }}</td>
                                    <td>
                                        <a href="{{ route('students.edit', $student->id) }}" title="Edit">
                                            <span class="edit"><i class="bi bi-pencil-square"></i></span>
                                        </a>
                                        <a href="{{ route('students.show', $student->id) }}" title="Details"><span
                                                class="eye"><i class="bi bi-eye"></i></span></a>
                                        <button data-bs-toggle="modal"
                                            data-bs-target="#deleteGuestModal{{ $student->id }}" title="Delete"
                                            class="deleteIcon">
                                            <span class="delete"><i class="bi bi-trash"></i></span>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No Instructor Found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                {{-- Adding custom pagination --}}
                {{ $students->links('pagination::custom') }}
            </div>
        </div>
    </main>
@endsection
@section('modals')
    @foreach ($students as $student)
        {{-- delete student confirmation modal --}}
        <div class="modal fade" id="deleteGuestModal{{ $student->id }}" tabindex="-1"
            aria-labelledby="deleteGuestModal{{ $student->id }}Label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteGuestModal{{ $student->id }}Label">Warning!</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {{-- form for deleting student --}}
                    <form id="deleteGuestForm" action="{{ route('students.destroy', $student->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <div class="modal-body">
                            Are you sure you want to delete this Teacher's data?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger">Yes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
