@extends('layouts.app', ['activePage' => 'dashboard', 'pageTitle' => __('Dashboard')])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
@endsection
@section('content')
    <main class="mian" id="main">
        {{-- chart added here --}}
        <canvas class="my-4 border rounded-5" id="myChart" width="400" height="200"></canvas>
        {{-- chart added here --}}
        {{-- <div class="row">
            <div class="col-md-4 col-12 mb-2">
                <div class="card1">
                    <div class="position-absolute top-0 end-0">
                        <img src="{{ asset('assets/img/Mask Group 1.png') }}" alt="">
                    </div>
                    <div class="icon">
                        <i class="bi bi-people"></i>
                    </div>
                    <div class="text">
                        <p>Total Instructors</p>
                        <h1>{{ $total_instructors }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12 mb-2">
                <div class="card2 m-md-auto">
                    <div class="cornerImage">
                        <img src="{{ asset('assets/img/Group 97.svg') }}" alt="">
                    </div>
                    <div class="icon">
                        <i class="bi bi-people"></i>
                    </div>
                    <div class="text">
                        <p>Present Instructors (Today)</p>
                        <!-- <p style="font-size: 11px">{{ \Carbon\Carbon::now()->format('d F Y') }}</p> -->
                        <h1>-----</h1>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card3 ms-md-auto">
                    <div class="cornerImage">
                        <img src="{{ asset('assets/img/Group 97.svg') }}" alt="">
                    </div>
                    <div class="icon">
                        <i class="bi bi-people"></i>
                    </div>
                    <div class="text">
                        <p>Absent Instructors (Today)</p>
                        <!-- <p style="font-size: 11px">{{ \Carbon\Carbon::now()->format('d F Y') }}</p> -->
                        <h1>-----</h1>
                    </div>
                </div>
            </div>

        </div> --}}
        {{-- <div class="row invoiceList my-4">
            <h2>New Instructor Added (Recently)</h2>
            <div class="table-responsive">
                <table class="table invoiceTable table-striped">
                    <thead>
                        <tr>
                            <th style="width: 5%">#</th>
                            <th style="width: 45%">Instructor Name</th>
                            <th style="width: 15%">Status</th>
                            <th style="35%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($instructors as $key => $instructor)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $instructor->name }}</td>
                                <td>
                                    <button class="present">
                                        Present
                                    </button>
                                </td>
                                <td>
                                    <a href="{{ route('instructors.show', $instructor->id) }}" title="Details"><span
                                            class="eye"><i class="bi bi-eye"></i></span>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">No Instructor Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div> --}}
    </main>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        // Chart.js initialization
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Present Instructors', 'Overall Instructors', 'Absent Instructors',
                    'Todays Total Entries'
                ],
                datasets: [{
                    label: 'Attendence of Instructors (CS & IT)',
                    data: [12, 19, 3, 5],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });



        // for checking
        // const contextData = document.getElementById('myChart').getContext('2d');

        // var chartData = new Chart(contextData, {
        //     type: 'bar',
        //     data: {
        //         labels: ['Presents One', 'Absent One', 'Total One', 'Total Absents'],
        //         datasets: [{
        //             label: '# of Votes',
        //             backgroundColor: ['rgba(209, 136, 75,0.852)', 'rgba(155, 111, 35,0.852)',
        //                 'rgba(89, 126, 158,0.852)',
        //                 'rgba(99, 36, 28,0.852)'
        //             ],
        //             borderColor: ['rgba(209, 136, 75,0.852)', 'rgba(155, 111, 35,0.852)',
        //                 'rgba(89, 126, 158,0.852)',
        //                 'rgba(99, 36, 28,0.852)'
        //             ],
        //             data: [10, 5, 20, 12],
        //             borderWidth: 1,
        //             fill: true,
        //             borderRadius: 5
        //         }]
        //     },
        //     options: {}
        // });
    </script>
@endsection
