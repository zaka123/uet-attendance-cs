@if ($paginator->hasPages())
    {{-- <div class="row"> --}}
    <div class="col-md-4 mt-3">
        @if ($paginator->total() > 0)
            <span>Showing {{ $paginator->firstItem() }} to {{ $paginator->lastItem() }} of {{ $paginator->total() }}
                records</span>
        @endif
    </div>
    <div class="col-md-8 mt-3">
        <nav>
            <ul class="pagination">
                <li class="page-item">
                    @if ($paginator->onFirstPage())
                        <a class="page-link" href="javascript:void(0)" aria-label="First" disabled>
                            <i class="bi bi-chevron-double-left"></i>
                        </a>
                    @else
                        <a class="page-link" href="{{ $paginator->url(1) }}" rel="first" aria-label="First">
                            <i class="bi bi-chevron-double-left"></i>
                        </a>
                    @endif
                </li>

                <li class="page-item">
                    @if ($paginator->onFirstPage())
                        <a class="page-link" href="javascript:void(0)" aria-label="Previous" disabled>
                            <i class="bi bi-chevron-left"></i>
                        </a>
                    @else
                        <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                            aria-label="Previous">
                            <i class="bi bi-chevron-left"></i>
                        </a>
                    @endif
                </li>

                @foreach ($elements as $element)
                    @if (is_string($element))
                        <li class="page-item disabled">{{ $element }}</li>
                    @endif

                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if (
                                $page == $paginator->currentPage() - 1 ||
                                    $page == $paginator->currentPage() ||
                                    $page == $paginator->currentPage() + 1)
                                <li class="page-item {{ $page == $paginator->currentPage() ? 'active' : '' }}">
                                    <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                <li class="page-item">
                    @if ($paginator->hasMorePages())
                        <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="Next">
                            <i class="bi bi-chevron-right"></i>
                        </a>
                    @else
                        <a class="page-link" href="javascript:void(0)" aria-label="Next" disabled>
                            <i class="bi bi-chevron-right"></i>
                        </a>
                    @endif
                </li>

                <li class="page-item">
                    @if ($paginator->hasMorePages())
                        <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" rel="last"
                            aria-label="Last">
                            <i class="bi bi-chevron-double-right"></i>
                        </a>
                    @else
                        <a class="page-link" href="javascript:void(0)" aria-label="Last" disabled>
                            <i class="bi bi-chevron-double-right"></i>
                        </a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>
    {{-- </div> --}}
@endif
