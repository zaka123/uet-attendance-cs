<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>invoice-{{ $invoice->invoice_no . '-' . $invoice?->student->name }}</title>
    <style>
        *,
        html,
        body {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .invoice_background_name {
            background-color: #BB1A1A;
            border-radius: 3px;
        }
    </style>
</head>

<body style="position: relative">
    <div style="padding:10px 20px">
        <!-- main logo here -->
        <div style="height:60px;width:750px;">
            <img src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('assets/img/logo-no-background.png'))) }}"
                style="width: 200px;height:60px;max-width:200px;object-fit:cover;">
        </div>
        <!-- invoice line here-->
        <div class="invoice_background_name" style="height:40px;width:750px;margin-top:30px">
            <h3
                style="text-align: center!important;padding-top:10px!important;color: white!important;opacity:1!important">
                INVOICE</h3>
        </div>
        <!--  invoice to -->
        <div style="margin-top: 20px">
            <h3>Invoice to:</h3>
            <div style="margin-top:10px">
                <p>{{ $invoice->student->name }}</p>
                <p>{{ $invoice->student->address }}</p>
                <p>{{ $invoice->student->phone }}</p>
            </div>
            <div style="margin-top:-85px;float:right">
                <h3>Invoice No. <span>{{ $invoice->invoice_no }}</span></h3>
                <div style="margin-top:10px">
                    <p>Date: <span>{{ date('d M Y', strtotime($invoice?->invoice_date)) }}</span></p>
                </div>
            </div>
        </div>
        <!-- description table  -->
        <div style="margin-top:50px">
            <table border="1" style="width: 750px;border-collapse:collapse;">
                <tr style="width: 750px;background-color:#BB1A1A">
                    <td style="height:40px!important;padding-left:2px;color:white;width:5%"><b>S.No</b></td>
                    <td style="height:40px!important;padding-left:10px;color:white;width:45%"><b>Description</b></td>
                    <td style="height:40px!important;text-align:center;color:white;width:15%"><b>Month</b></td>
                    <td style="height:40px!important;text-align:center;color:white;width:15%"><b>Year</b></td>
                    <td style="height:40px!important;text-align:center;color:white;width:20%"><b>Amount</b></td>
                </tr>
                @foreach ($invoice->invoiceItems as $invoiceItem)
                    <tr style="width: 750px;">
                        <td style="height:40px!important;padding-left:10px">{{ $loop->iteration }}</td>
                        <td style="height:40px!important;padding-left:10px">{{ $invoiceItem->description ?? '-' }}</td>
                        <td style="height:40px!important;text-align:center">{{ $invoiceItem->month ?? '-' }}</td>
                        <td style="height:40px!important;text-align:center">{{ $invoiceItem->year ?? '-' }}</td>
                        <td style="height:40px!important;text-align:center">{{ $invoiceItem->price }} PKR</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- terms and conditions -->

        <div style="margin-top:30px">
            <p>Thank you for your bussiness</p>
            <h3 style="margin-top:30px">Terms & Conditions</h3>
            <p style="margin-top: 10px">Payment is due within 5 days from the date of issue</p>
        </div>

        <!-- total and subtotal -->
        <div style="float: right;margin-top:-30px;background-color:rgb(241, 196, 15);padding:10px 20px">
            <h3>Total: <span>{{ $invoice->total_amount }} PKR</span></h3>
        </div>

        <!--  payment info -->
        <div>
            <h3 style="margin-top: 50px">Bank Details</h3>
            <p style="margin-top:10px"><b>Bank Name : </b>HBL</p>
            <p style="margin-top:10px"><b>Account Title : </b>Umair</p>
            <p style="margin-top:10px"><b>Account No. </b>1251215126785</p>
        </div>

        <!-- signature place -->
        <div style="float: right;margin-top:-30px">
            <p>___________________________</p>
            <p style="text-align: center">Authorised Signature</p>
        </div>


    </div>
    <!-- footer place -->
    <div style="position: absolute; bottom:0; width: 800px">
        {{-- <hr> --}}
        <div style="margin:auto; width: 800px; font-size: 16px; color: white; background-color: #BB1A1A">
            <p style="text-align: center; padding: 14px"><strong>WHITE INN GUEST HOUSE OPP CUSTOM OFFICE PESHAWAR - 03190449595 - 03340449595</strong></p>
        </div>
    </div>
</body>

</html>
