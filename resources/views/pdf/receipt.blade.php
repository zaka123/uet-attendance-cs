<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>invoice-{{ $invoice->id . '-' . $invoice?->customer_name }}</title>
    <!--Bootstrap CSS Files -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css"
        integrity="sha384-b6lVK+yci+bfDmaY1u0zE8YYJt0TZxLEAFyYSLHId4xoVvsrQu3INevFKo+Xir8e" crossorigin="anonymous">
    <!--Main CSS File -->
    <link href="{{ asset('assets/css/receipt.css') }}" rel="stylesheet">
</head>

<body>
    <div class="receipt_buttons">
        <a class="print-btn" href="{{ route('invoices.pdfView', $invoice->id) }}" target="_blank">Print</a>
        <a class="print-btn" style="background-color: #8f8a8a;" href="{{ route('invoices.index') }}">Invoice
            List</a>
    </div>
    <div class="container">
        <div class="receipt_logo">
            <img src="{{ asset('assets/img/logo.svg') }}" alt="">
        </div>
        <div class="hero_text">
            <h2>DRIVER FOR CAR RENTAL</h2>
            <h1>مؤسسة سائق بالسيارة لتأجير السيارات</h1>
        </div>
        <div class="boxes pb-4">
            <div class="box">
                <p>Invoice Number</p>
                <small>#00{{ $invoice->id }}</small>
            </div>
            <div class="box">
                <p>Invoice Date</p>
                <small>{{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</small>
            </div>
            <div class="box">
                <p>Invoice Time</p>
                <small>{{ date('h:i A', strtotime($invoice->invoice_date)) }}</small>
            </div>
            <div class="box">
                <p>Terms</p>
                <small>Due on Receipt</small>
            </div>
        </div>
        <div class="boxes_2 mt-4">
            <div class="box_2">
                <small>From</small>
                <div class="Driver p-3">
                    <h1>DRIVER FOR CAR RENTAL</h1>
                    <p>Address</p>
                    <small>Saudi Arabia Taif</small>
                    <div class="contact_ancestor">
                        <div>
                            <h6 class="number_p">Contact Number</h6>
                            <h6 class="number_s">5454353551</h6>
                        </div>
                        <div>
                            <h6 class="registration_p">Company Registration Number</h6>
                            <h6 class="registration_s">5454353551</h6>
                        </div>
                    </div>
                </div>
            </div>
            {{-- php code for formating data for qr code --}}
            @php
                $total = 0;
                foreach ($invoice?->invoiceItems as $item) {
                    $total += $item->price * $item->quantity;
                }
                if ($invoice->tax > 0) {
                    $total += $total * ($invoice->tax / 100);
                }

                $data = [
                    'Supplier GSTIN' => $invoice->tax_number ?? '',
                    'Recepient GSTIN' => 310296660800003,
                    'Document Number' => $invoice->id,
                    'Document Type' => 'Invoice',
                    'Document Date' => date('d M Y', strtotime($invoice?->invoice_date)),
                    'Number of Items' => $invoice?->invoiceItems->count() ?? 0,
                    'Total Invoice Value' => 'AED ' . number_format($total, 2),
                ];

                // Format the data into a simple text format
                $textData = 'Supplier GSTIN: ' . $data['Supplier GSTIN'] . "\n" . 'Recepient GSTIN: ' . $data['Recepient GSTIN'] . "\n" . 'Document Number: ' . $data['Document Number'] . "\n" . 'Document Type: ' . $data['Document Type'] . "\n" . 'Document Date: ' . $data['Document Date'] . "\n" . 'Number of Items: ' . $data['Number of Items'] . "\n" . 'Total Invoice Value: ' . $data['Total Invoice Value'];
            @endphp

            {{-- generate qr code --}}
            <div class="text-center pt-5" style="width: 200px;">
                {{ \SimpleSoftwareIO\QrCode\Facades\QrCode::size(160)->generate($textData) }}
            </div>

            <div class="box_2">
                <small>To</small>
                <div class="Driver p-3">
                    <h1>DRIVER FOR CAR RENTAL</h1>
                    <p>Address</p>
                    <small>{{ $invoice->address ?? '' }}</small>
                    <div class="contact_ancestor">
                        <div>
                            <h6 class="number_p">Contact Number</h6>
                            <h6 class="number_s">{{ $invoice?->phone }}</h6>
                        </div>
                        <div>
                            <h6 class="registration_p">Tax Number</h6>
                            <h6 class="registration_s">{{ $invoice?->tax_number }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-border mt-4">
            <thead>
                <tr>
                    <th class="ps-5">#</th>
                    <th class="text-start" style="padding-left:190px">Item & Descripton</th>
                    <th class="pe-4">Service Type</th>
                    <th class="pe-5">Quantity</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($invoice->invoiceItems as $key => $item)
                    <tr>
                        <td class="ps-5">{{ $key + 1 }}</td>
                        <td class="text-start " style="padding-left:190px">
                            {{ $item?->car_type }}<br><span>{{ $item?->description }}</span></td>
                        <td>{{ $item?->service_type }}</td>
                        <td class="pe-5">{{ $item->quantity }}</td>
                        <td>SAR {{ number_format($item->price, 2) }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No items</td>
                    </tr>
                @endforelse

            </tbody>
        </table>
        <div class="TermSection mt-5">
            <div class="terms">
                <h1>Payment Terms:</h1>
                <p>Payment is Due 5 Days form issue on {{ date('d-m-Y', strtotime($invoice->invoice_date)) }}</p>
            </div>
            <div class="stacks">
                <div class="stack">
                    <p>Sub Total</p>
                    <small>SAR {{ number_format($invoice->total_amount, 2) }}</small>
                </div>
                <div class="stack">
                    <p>Tax</p>
                    <small>{{ $invoice->tax > 0 ? '15%' : '0%' }}</small>
                </div>
                <div class="stack">
                    <p>Total Amount</p>
                    <small>SAR
                        {{ number_format($invoice->total_amount + $invoice->total_amount * ($invoice->tax / 100), 2) }}</small>
                </div>
            </div>
        </div>
        <div class="bankSection mt-5">
            <h1>Bank Details</h1>
            <div class="boxes_3 ">
                <div class="box">
                    <p style="padding-right: 30px">Bank Name</p>
                    <small>Al Rajhi Banking System</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">Account Name</p>
                    <small>Car Rental</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">Account Number</p>
                    <small>4757865645161</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">IBAN Number</p>
                    <small>4757865645161</small>
                </div>
            </div>
        </div>
        <div class="companySection mt-5">
            <h1>Company Information</h1>
            {{-- generate qr code --}}
            <div class="text-center" style="width: 200px;background-color: #c52727">
                {{ \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($textData) }}
            </div>
            {{-- end generate qr code --}}
            <div class="boxes_3 ">
                <div class="box">
                    <p style="padding-right: 30px">Official Address</p>
                    <small>Saudi Arabia Al Taif</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">Mobile Number</p>
                    <small>+966 5454353551</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">Company Registration Number</p>
                    <small>4757865645161</small>
                </div>
                <div class="box">
                    <p style="padding-right: 30px">VAT Registration Tax Number</p>
                    <small>4757865645161</small>
                </div>
            </div>
        </div>
        <footer class="p-3">
            <h1>CAR DRIVER OF CAR RENTAL</h1>
            <p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate<br> the
                visual form of a document or a typeface without relying on meaningful content</p>
        </footer>
    </div>
</body>

</html>
