<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>invoice-{{ $invoice->id . '-' . $invoice?->customer_name }}</title>
    {{-- <link rel="stylesheet" href="public/css/style.css" /> --}}
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            /* border-bottom: none; */
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .invoice-box.rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .invoice-box.rtl table {
            text-align: right;
        }

        .invoice-box.rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="title">
                                {{-- qr code image --}}
                                <div style="width: 100%; max-width: 88px;">
                                    <img src="data:image/png;base64,{{ base64_encode(file_get_contents(base_path() . '/public/storage/qrcodes/qrcode-' . $invoice->id . '.png')) }}"
                                        style="width: 100%; max-width: 88px;" />
                                </div>
                            </td>
                            <td>
                                <strong>Invoice #:</strong> {{ $invoice->id }}<br />
                                <strong>Date:</strong> {{ date('d M Y', strtotime($invoice->invoice_date)) }} <br>
                                <strong>Status:</strong> {{ $invoice->status }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="text-start">
                                Company, LLC.<br />
                                12345 Example Road<br />
                                Example, EX 12345
                            </td>
                            <td>
                                {{ $invoice->customer_name }}<br />
                                {{ $invoice->phone }}<br />
                                {{ $invoice->address }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="heading">
                <td>#</td>
                <td class="text-center">Description</td>
                <td>Car Type</td>
                <td>Service Type</td>
                <td>Quantity</td>
                <td>Price</td>
            </tr>
            @php
                $i = 1;
            @endphp
            @foreach ($invoice->invoiceItems as $invoiceItem)
                <tr class="item @if ($loop->last) last @endif">
                    <td>{{ $i++ }}</td>
                    <td class="text-center">{{ $invoiceItem->description }}</td>
                    <td>{{ $invoiceItem->car_type }}</td>
                    <td>{{ $invoiceItem->service_type }}</td>
                    <td>{{ $invoiceItem->quantity }}</td>
                    <td>${{ number_format($invoiceItem->price, 2) }}</td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Total: ${{ number_format($invoice->total_amount, 2) }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Tax({{ $invoice->tax }}%):
                    ${{ $invoice->tax > 0 ? number_format($invoice->total_amount * ($invoice->tax / 100), 2) : 0 }}
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Sub Total:
                    ${{ number_format($invoice->total_amount + $invoice->total_amount * ($invoice->tax / 100), 2) }}
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
