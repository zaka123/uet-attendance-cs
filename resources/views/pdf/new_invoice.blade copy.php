<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>invoice-{{ $invoice->invoice_no . '-' . $invoice?->customer_name }}</title>

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap');

        @font-face {
            font-family: Arial;
            src: url('{{ asset('assets/css/ARIAL.ttf') }}');
            /* src: url('{{ public_path() . '/assets/css/ARIAL.ttf' }}'); */
        }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        /* @font-face {
            font-family: 'Amiri';
        } */

        .invoice-box {
            max-width: 100%;
            height: 95% !important;
            margin: auto;
            border: 1px solid #eee;
        }

        .header {
            width: 100%;
            height: 40px;
            background-color: #BB1A1A;
        }

        header {
            width: 100%;
            position: relative;
        }

        h1 {
            /* font: normal normal bold 22px/18px DummyFont; */
            letter-spacing: 0px;
            color: #000000;
            text-transform: uppercase;
            opacity: 1;
            text-align: center;
        }

        h2 {
            /* font: normal normal 600 21px/20px poppins; */
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
            text-align: center;
        }

        h3 {
            font: normal normal bold 17px/10px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: uppercase;
            opacity: 1;
        }

        h4 {
            font: normal normal bold 12px/12px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
        }

        h5 {
            font: normal normal bold 13px/0px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: capitalize;
            opacity: 1;
        }

        p {
            font: normal normal normal 13px/20px sans-serif;
            letter-spacing: 0px;
            color: #646464;
            opacity: 1;
            padding: 0px 15px !important;
            text-align: left;
        }

        nav {
            padding: 30px;
            height: 80px;
            position: relative;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center
        }

        nav .logo img {
            width: 137px;
            height: 137px;
            filter: invert(1)!important;
        }

        .invoiceTo {
            padding: 45px 45px 20px;
            position: relative;
            margin-top: 80px;
        }

        .invoiceTo .qr_code {
            width: 150px;
            height: 200px;
            text-align: left;
            position: absolute;
            top: -3%;
            right: 3%;
        }

        table {
            font-family: sans-serif;
            border-collapse: collapse;
            width: 90%;
            margin: auto;
        }

        th {
            background-color: #BB1A1A;
            color: white;
            font: normal normal normal 13px/29px sans-serif;
            letter-spacing: 0px;
            color: #FFFFFF;
            text-transform: capitalize;
            opacity: 1;
            margin-bottom: 25px !important;
        }

        tbody:before {
            content: "-";
            display: block;
            line-height: 5px;
            color: transparent;
        }

        td {
            font: normal normal normal 13px/29px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: capitalize;
            opacity: 1;
        }

        tbody tr {
            border: 1px solid rgb(169, 169, 169) !important;
            border-radius: 3px !important;
        }

        td,
        th {
            border: none !important;
            text-align: left;
            padding: -5px 5px 5px !important;
        }

        .Amount {
            background: #F1F1F1 0% 0% no-repeat padding-box;
            margin-top: 20px;
            width: 300px;
            /* margin-left: auto; */
            margin-right: 40px;
        }

        .terms {
            margin: 0px 45px;
            width: 200px;
        }

        .banks_ancestor {
            width: 400px;
            height: 115px;
            background: #F1F1F1 0% 0% no-repeat padding-box;
            margin: 10px 45px 50px;
            padding: 10px;
            padding-left: 30px !important;
        }

        #footer_table {
            background: transparent;
            padding: 0px 25px !important;
            margin: 0px !important;
        }

        #footer_table td {
            color: white !important;
        }

        .footer {
            width: 100%;
            height: 100px;
            background-color: #BB1A1A;
            position: fixed;
            bottom: 0
        }

        .footer-left {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div class="header"></div>
        <header class="">
            <h1>WHITE HOUSE GROUP OF HOSTELS</h1>
        </header>
        <nav style="width:200px">
            <div class="logo">
                <img src="data:image/png;base64,{{ base64_encode(file_get_contents(base_path() . '/public/assets/img/Screenshot__222_-removebg-preview.png')) }}"
                    alt="" />
            </div>
            <div>
                <p>Saudi Arabia<br>Taif - riha 26563</p>
                <p>Info@luxurytransportksa.com</p>
                <p>+966537778929 </p>
            </div>
        </nav>
        <div class="invoiceTo">
            <div>
                <h3 style="margin-bottom: 7px!important">invoice to</h3>
                <p style="padding:0px 0px!important;">{{ $invoice?->customer_name }}</p>
                <p style="padding:0px 0px!important;width:200px!important">{{ $invoice?->address }}</p>
                <p style="padding:0px 0px!important">{{ $invoice?->phone }}</p>
                <h4 style="margin-top:12px">Tax Registration Number</h4>
                <p style="padding:0px 0px!important;line-height:2!important">{{ $invoice?->tax_number }}</p>
            </div>
            <div class="qr_code">
                <h3 style="padding-left:9px!important">invoice no. {{ $invoice->invoice_no }}</h3>
                <p style="line-height:2!important;padding-left:22px!important;margin-bottom:12px">Date:
                    {{ date('d M Y', strtotime($invoice?->invoice_date)) }}</p>
                {{-- <img src="data:image/png;base64,{{ base64_encode(file_get_contents(base_path() . '/public/storage/qrcodes/qrcode-' . $invoice->id . '.png')) }}"
                    alt=""
                    style="width: 100%; max-width: 100px; height: 100%; max-height: 100px;margin-left:25px"> --}}
            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <!--<th>#</th>-->
                    <th style="text-align:left">Descripton</th>
                    <th style="text-align:center">Car Type</th>
                    <th style="text-align:center">Service Type</th>
                    <th style="text-align:center">Quantity</th>
                    <th style="text-align:center">Price</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @if ($invoice->invoiceItems)
                    @foreach ($invoice->invoiceItems as $invoiceItem)
                        <tr>
                            <!--<td>{{ $i++ }}</td>-->
                            <td>{{ $invoiceItem->description }}</td>
                            <td style="text-align:center">{{ $invoiceItem->car_type }}</td>
                            <td style="text-align:center">{{ $invoiceItem->service_type }}</td>
                            <td style="text-align:center">{{ $invoiceItem->quantity }}</td>
                            <td style="text-align:center">SAR {{ number_format($invoiceItem->price, 2) }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <table style="width:300px!important;" class="Amount">
            <thead>
                <tr>
                    <td style="font-weight: bold;width:100px;padding:0px 8px!important;">
                        Sub Total
                    </td>
                    <td style="font-size: 13px;width:200px!important;text-align:right;padding:0px 20px!important;">
                        SAR {{ $invoice?->total_amount }}</td>
                </tr>
            </thead>
            <tbody>
                <tr style="border: none!important;">
                    <td style="font-weight: bold;width:100px;padding:8px 8px!important">Tax (15%)</td>
                    <td style="font-size: 13px;width:200px!important;text-align:right;padding:0px 20px!important;">SAR
                        {{ number_format(($invoice?->tax / 100) * $invoice?->total_amount, 2) }}</td>
                </tr>
                <tr style="border: none!important;background-color:#BB1A1A;">
                    <td
                        style="color: white;font-weight: bold;width:100px;padding:5px 8px!important;padding-bottom:15px!important">
                        Total
                        Amount
                    </td>
                    <td
                        style="font-size: 13px;width:200px!important;text-align:right;padding:5px 20px!important;color:white;padding-bottom:15px!important">
                        SAR
                        {{ number_format($invoice?->total_amount + ($invoice?->tax / 100) * $invoice?->total_amount, 2) }}
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="width:300px;margin-top:-60px">
            <div class="terms">
                <h4 style="line-height:1.1!important;">Payment Terms</h4>
                <p style="padding: 4px 0px!important">Payment is Due 5 Days from issue on
                    {{ date('d-m-Y', strtotime($invoice?->invoice_date)) }}</p>
            </div>
            <h4 style="padding:6px 45px!important;width:100px!important;margin:0px 2px!important">Bank Details</h4>
        </div>
        <table class="banks_ancestor">
            <thead>
                <tr>
                    <td style="padding: 0!important;font-size:14px;font-weight:bold">
                        Bank Name
                    </td>
                    <td style="padding: 0!important;font-size:14px;font-weight:bold;padding-left:20px!important">
                        Account Number
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0!important;line-height:1.4!important">AL Rajhi Banking System</td>
                    <td style="padding: 0!important;line-height:1.4!important;padding-left:20px!important">4757865645161
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr style="border: none!important">
                    <td style="padding: 0!important;font-size:14px;font-weight:bold">
                        Account Name
                    </td>
                    <td style="padding: 0!important;font-size:14px;font-weight:bold;padding-left:20px!important">
                        IBAN Number
                    </td>
                </tr>
                <tr style="border: none!important">
                    <td style="padding: 0!important;line-height: 1.4!important">Car Driver For Car Rental</td>
                    <td style="padding: 0!important;line-height: 1.4!important;padding-left:20px!important">
                        SA1280000477608016395448</td>
                </tr>
            </tbody>
        </table>
        <div class="footer">
            <div class="footer-left">
                <table class="banks_ancestor" id="footer_table" style="width:100%;margin:20px!important;">
                    <thead>
                        <tr>
                            <td
                                style="padding: 0!important;font-size:14px;font-weight:bold;width:70%!important;text-align:center">
                                Company Registration Number
                            </td>
                            <td
                                style="padding: 0!important;font-size:14px;font-weight:bold;padding-left:20px!important;width:100%!important;text-align:center">
                                Vat Registration Tax Number
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0!important;line-height:1.4!important;text-align:center">4032232767</td>
                            <td
                                style="padding: 0!important;line-height:1.4!important;padding-left:20px!important;text-align:center">
                                310296660800003
                            </td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
