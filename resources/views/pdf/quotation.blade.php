<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>quotation-{{ $quotation->id . '-' . $quotation?->customer_name }}</title>

    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap');

        @font-face {
            font-family: Arial;
            src: url('{{ asset('assets/css/ARIAL.ttf') }}');
            /* src: url('{{ public_path() . '/assets/css/ARIAL.ttf' }}'); */
        }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        /* @font-face {
            font-family: 'Amiri';
        } */

        .invoice-box {
            max-width: 100%;
            height: 95% !important;
            margin: auto;
            border: 1px solid #eee;
        }

        .header {
            width: 100%;
            height: 40px;
            background-color: #BB1A1A;
        }

        header {
            width: 307px;
            position: relative;
        }

        header img {
            width: 100%;
            position: absolute;
            top: 2%;
            left: 80%;
        }

        h1 {
            /* font: normal normal bold 22px/18px DummyFont; */
            letter-spacing: 0px;
            color: #000000;
            text-transform: uppercase;
            opacity: 1;
            text-align: center;
        }

        h2 {
            /* font: normal normal 600 21px/20px poppins; */
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
            text-align: center;
        }

        h3 {
            font: normal normal bold 17px/10px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: uppercase;
            opacity: 1;
        }

        h4 {
            font: normal normal bold 12px/12px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
        }

        h5 {
            font: normal normal bold 13px/0px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: capitalize;
            opacity: 1;
        }

        p {
            font: normal normal normal 13px/20px sans-serif;
            letter-spacing: 0px;
            color: #646464;
            opacity: 1;
            padding: 0px 15px !important;
            text-align: left;
        }

        nav {
            padding: 30px;
            height: 80px;
            position: relative;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center
        }

        nav .logo img {
            width: 137px;
            height: 137px;
        }

        .invoiceTo {
            padding: 45px 45px 20px;
            position: relative;
            margin-top: 90px;
        }

        .invoiceTo .qr_code {
            width: 150px;
            height: 200px;
            text-align: left;
            position: absolute;
            top: -3%;
            right: 3%;
        }

        table {
            font-family: sans-serif;
            border-collapse: collapse;
            width: 90%;
            margin: auto;
        }

        th {
            background-color: #BB1A1A;
            color: white;
            font: normal normal normal 13px/29px sans-serif;
            letter-spacing: 0px;
            color: #FFFFFF;
            text-transform: capitalize;
            opacity: 1;
            margin-bottom: 25px !important;
        }

        tbody:before {
            content: "-";
            display: block;
            line-height: 5px;
            color: transparent;
        }

        td {
            font: normal normal normal 13px/15px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            text-transform: capitalize;
            opacity: 1;
        }

        tbody tr {
            border: 1px solid rgb(169, 169, 169) !important;
            border-radius: 3px !important;
        }

        td,
        th {
            border: none !important;
            text-align: left;
            padding: 8px !important;
            width:14%!important;
        }

        th {
            font-weight: bold;
        }

        .terms_condition {
            padding: 45px !important;
        }

        .terms_condition ol {
            padding: 15px 20px !important;
        }

        .terms_condition ol li {
            padding: 3px 0px !important;
            font: normal normal normal 14px/14px sans-serif;
            letter-spacing: 0px;
            color: #000000E3;
            opacity: 1;
        }

        .terms_condition small {
            font: normal normal normal 12px/0px sans-serif;
            letter-spacing: 0px;
            color: #000000BA;
            opacity: 1;
        }

        .terms_condition h4 {
            font: normal normal bold 17px/0px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
            margin-top: 60px;
        }

        .terms_condition a {
            font: normal normal bold 15px/18px sans-serif;
            letter-spacing: 0px;
            color: #3A60FA;
            display: inline-block;
            opacity: 1;
            text-decoration: none;
        }
    .terms_condition p {
         margin-top:20px!important;
        }
        .terms_condition h2 {
            text-align: left !important;
            font: normal normal bold 21px/36px sans-serif;
            letter-spacing: 0px;
            color: #000000;
            opacity: 1;
        }

        #footer_table {
            background: transparent;
            padding: 0px 25px !important;
            margin: 0px !important;
        }

        #footer_table td {
            color: white !important;
        }

        .footer {
            width: 100%;
            height: 100px;
            background-color: #BB1A1A;
            position: fixed;
            bottom: 0
        }

        .footer-left {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div class="header"></div>
        <header>
            <img src="data:image/png;base64,{{ base64_encode(file_get_contents(base_path() . '/public/assets/img/WhatsApp_Image_2023-10-19_at_17.05.05_4a33bc83-removebg-preview.png')) }}"
                alt="" />
        </header>
        <nav style="width:200px">
            <div class="logo">
                <img src="data:image/png;base64,{{ base64_encode(file_get_contents(base_path() . '/public/assets/img/logo-2.png')) }}"
                    alt="" />
            </div>
            <div>
                <h3 style="padding-left: 15px">Info:</h3>
                <p>Ibrahim Al hilali Street 13236<br>
Saudi Arabia Riyadh</p>
                <p>Info@luxurytransportksa.com
</p>
                <p>+966537778929 </p>
            </div>
        </nav>
        <div class="invoiceTo">
            <div>
                <h3 style="margin-bottom: 3px!important">Quote to:</h3>
                <p style="padding:0px 0px!important;">{{ $quotation?->customer_name }}</p>
                <p style="padding:0px 0px!important;width:200px!important">{{ $quotation?->address }}</p>
                <p style="padding:0px 0px!important">{{ $quotation?->phone }}</p>
            </div>
            <div class="qr_code">
                <h3></h3>
                <p style="line-height:2!important;padding-left:3px!important;margin-top:122px">Date:
                    {{ date('d M Y', strtotime($quotation?->quotation_date)) }}</p>

            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <!--<th>#</th>-->
                    <th>Descripton</th>
                    <th style="text-align:center">Qty</th>
                    <th style="text-align:center">Hourly</th>
                    <th style="text-align:center">Hour/O.T</th>
                    <th style="text-align:center">Full Day</th>
                    <th style="text-align:center">Hours</th>
                    <th style="text-align:center">P2P Transfer</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @if ($quotation->quotationItems)
                    @foreach ($quotation->quotationItems as $item)
                        <tr>
                            <!--<td>{{ $i++ }}</td>-->
                            <td>{{ $item->description }}</td>
                            <td style="text-align:center;">{{ $item->quantity }}</td>
                            <td style="text-align:center">{{ $item->hourly }}</td>
                            <td style="text-align:center">{{ $item->hourly_ot }}</td>
                            <td style="text-align:center">{{ $item->full_day }}</td>
                            <td style="text-align:center">{{ $item->no_of_hours }}</td>
                            <td style="padding-left:20px!important;text-align:center">{{ $item->arp_transfer }} </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <div class="terms_condition">
            <h2>Terms & Conditions</h2>
            <ol>
                <li>Customer will be billed after indicating acceptance of this quote</li>
                <li>Please fax or mail the signed price quote to the address above</li>
                <li>The Prices Included no hidden charges, and does not Include TAX (15%)</li>
            </ol>
            <small>Customer Acceptance (sign below):</small>
            <h4>Signature:_______________</h4>
            <p style="padding-left: 0px !important;margin-top:7px">If you have any questions about this Quotation, please contact Us</p>
            <a href="#">info@luxurytransportksa.com</a>
        </div>
        <div class="footer">
            <div class="footer-left">
                <table class="banks_ancestor" id="footer_table" style="width:100%;margin:20px!important;">
                    <thead>
                        <tr>
                            <td
                                style="padding: 0!important;font-size:14px;font-weight:bold;width:70%!important;text-align:center">
                                Company Registration Number
                            </td>
                            <td
                                style="padding: 0!important;font-size:14px;font-weight:bold;padding-left:20px!important;width:100%!important;text-align:center">
                                Vat Registration Tax Number
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0!important;line-height:1.4!important;text-align:center">4032232767</td>
                            <td
                                style="padding: 0!important;line-height:1.4!important;padding-left:20px!important;text-align:center">
                                310296660800003
                            </td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
