<!--Bootstrap CSS Files -->
<link href="{{ asset('assets/Bootstrap/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/Bootstrap/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
<!-- Toastr CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css">
<!--Main CSS File -->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
