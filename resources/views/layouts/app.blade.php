<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}" type="image/x-icon">
    <!-- Open Graph meta tags for social media sharing -->
    <meta property="og:title" content="UET CS & IT ATTENDENCE">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta property="og:description" content="UET CS & IT ATTENDENCE">
    <meta property="og:image" content="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}">
    <meta property="og:url" content="{{ url('/') }}">

    <!-- Twitter Card meta tags for Twitter sharing -->
    <meta name="twitter:card" content="UET CS & IT ATTENDENCE">
    <meta name="twitter:title" content="UET CS & IT ATTENDENCE">
    <meta name="twitter:description" content="UET CS & IT ATTENDENCE">
    <meta name="twitter:image" content="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}">

    <title>UET CS & IT- {{ $pageTitle ?? '' }}</title>

    @include('layouts.includes.head-links')

    @yield('css')
</head>

<body>
    <!-- ======= Modals ======= -->
    @yield('modals')
    <!-- ======= End Modals ======= -->
    <!-- ======= Header ======= -->
    <header id="header" class="header d-flex align-items-center me-auto ms-md-auto me-md-0  pe-5 pt-4">
        <nav class="header-nav ms-auto">
            <ul class="d-flex align-items-center">
                <!-- Search Bar -->
                {{-- <li>
                    <form id="searchForm" action="{{ route('search') }}" method="get">
                        <div class="search-bar">
                            <i class="bi bi-search"></i>
                            <input type="text" placeholder="Search" name="query"
                                onkeydown="if(event.keyCode == 13) { document.getElementById('searchForm').submit(); }">
                        </div>
                    </form>
                </li> --}}
                <!-- End Search Bar -->
                <li>
                    <div class="user-profile p-1">
                        <img src="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}" alt="">
                        <div class="user-info">
                            <p style="font-size: 9px">Mr. Syed Adeel Ali Shah</p>
                            <small>Chairman CS & IT</small>
                        </div>
                    </div>
                </li>
                <li class="settingIcon">
                    <a href="{{ route('password.change') }}" class="border-0" title="Change Password">
                        <i class="bi bi-gear"></i>
                    </a>
                </li>
                <li class="logoutIcon">
                    <a href="{{ route('logout') }}" title="Logout"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="bi bi-box-arrow-right"></i>
                    </a>
                    {{-- logout-form --}}
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                <!-- for closing and opening sidebar -->
                <li><i class="bi bi-list toggle-sidebar-btn"></i></li>
            </ul>
        </nav>
    </header>
    <!-- ======= Sidebar ======= -->
    <aside id="sidebar" class="sidebar">

        <ul class="sidebar-nav" id="sidebar-nav">
            <li class="mainLogo d-flex align-items-center justify-content-center">
                <a href="{{ Route('dashboard') }}">
                    <img class="" src="{{ asset('assets/img/UET-ASSETS/uet-logo.png') }}" alt="">
                </a>
            </li>
            <li class="my-5 text-center">
                <p>{{ \Carbon\Carbon::now()->format('d F Y') }}</p> {{-- d M Y => 12 Mar 2023 --}} {{-- d F Y => 12 March 2023 --}}
                <h1>{{ \Carbon\Carbon::now()->format('h:i') }}<span>{{ \Carbon\Carbon::now()->format('A') }}</span>
                </h1>
            </li>
            <li>
                <div class="page1">
                    <a href="{{ route('dashboard') }}"
                        class="button {{ $activePage == 'dashboard' ? 'active' : '' }}">
                        <i class="bi bi-house-door"></i>
                        <span class="ms-3">Dashboard</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="page3">
                    <a href="{{ route('instructors.index') }}"
                        class="button @if ($activePage == 'teachers') active @endif">
                        <i class="bi bi-people"></i>
                        <span class="ms-3">Instructors</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="page3">
                    <a href="#" class="button @if ($activePage == 'attendance') active @endif">
                        <i class="bi bi-person-check"></i>
                        <span class="ms-3">Daily Attendance</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="page3">
                    <a href="{{ route('sheets.index') }}" class="button @if ($activePage == 'googleSheets') active @endif">
                        <i class="bi bi-list-task"></i>
                        <span class="ms-3">Google sheets</span>
                    </a>
                </div>
            </li>
        </ul>

    </aside><!-- End Sidebar-->
    <main>
        @yield('content')
    </main>
    <!--JS Files -->
    @include('layouts.includes.js-scripts')

    @yield('scripts')

</body>

</html>
