@extends('layouts.user', ['activePage' => 'teachers', 'pageTitle' => __('Teachers')])
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}">
    <style>
        .plus-icon {
            padding: 4px 7px;
            /* background-color: #17c914; */
            background-color: #198754;
            border-radius: 9px;
            margin-right: 5px !important;
            padding-bottom: 6px !important;
            color: white;
        }

        .btn-check:checked+.btn {
            background-color: #D0874C;
        }

        .btn-outline-primary {
            border: 1px solid black !important;
            outline: none !important;
            box-shadow: none !important;
        }

        .btn-outline-primary:focus {
            border: 1px solid black !important;
            outline: none !important;
            box-shadow: none !important;
        }

        .btn-outline-primary:hover {
            border: 1px solid black !important
        }

        .btn-check+.btn {
            color: black;
        }

        .btn-check:checked+.btn {
            color: white !important;
        }

        .main-box {
            max-width: 100%;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;

        }

        .decoBrush {
            background-image: url('/assets/img/UET-ASSETS/deco-brush.svg');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
    </style>
@endsection
@section('content')
    <main id="main" style="overflow: auto!important">
        <div class="main-box bg-white mb-5 rounded-5">
            <div class="row invoiceList mt-2">
                <div class="col-md-6 my-3">
                    <h2>Hello 👋 Mr. <span class="text-uppercase decoBrush py-2 px-4">{{ auth()->user()->name }}</span>
                    </h2>
                </div>
                <div style="overflow:auto">
                    <iframe
                        src="https://docs.google.com/spreadsheets/d/1Qgvycw0W3DR2xTuxUM6hXS_FL7LP2F_u3rSHhfuKe0c/edit?usp=sharing"
                        frameborder="0" width="100%" height="800"></iframe>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script>
        var present = document.getElementById('present');
        var absent = document.getElementById('absent');
        var presentLabel = document.getElementById('presentLabel');
        var absentLabel = document.getElementById('absentLabel');

        presentLabel.addEventListener('mouseover', function() {
            if (present.checked == false && absent.checked == false) {
                presentLabel.style.color = 'black';
                absentLabel.style.color = 'black';
            }
        });
    </script>
@endsection
