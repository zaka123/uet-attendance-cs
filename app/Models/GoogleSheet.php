<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleSheet extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'course_name', 'sheet_link', 'month'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
