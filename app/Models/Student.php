<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'father_name',
        'cnic',
        'email',
        'phone',
        'address',
        'emergency_1',
        'emergency_2',
        'room_id',
        'status',
        'joining_date',
        'leaving_date',
    ];

    // public function getFullNameAttribute()
    // {
    //     return $this->name . ' ' . $this->father_name;
    // }
}
