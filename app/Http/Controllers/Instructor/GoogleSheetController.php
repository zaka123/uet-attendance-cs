<?php

namespace App\Http\Controllers\Instructor;

use App\Http\Controllers\Controller;
use App\Models\GoogleSheet;
use Illuminate\Http\Request;

class GoogleSheetController extends Controller
{
    public function index()
    {
        $googleSheets = GoogleSheet::orderBy('id', 'desc')->where('user_id', auth()->user()->id)->paginate(5);
        return view('googleSheets.index', compact('googleSheets'));
    }

    public function create()
    {
        return view('googleSheets.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'course_name' => 'required',
            'sheet_link' => 'required',
            'month' => 'required',
        ]);

        $validated['user_id'] = auth()->user()->id;
        GoogleSheet::create($validated);
        // return back()->with('error', 'Failed to save record');
        return redirect()->route('user.sheets.index')->with('success', 'Record saved successfully');
    }

    public function show($id)
    {
        $googleSheet = GoogleSheet::findOrFail($id);
        return view('googleSheets.show', compact('googleSheet'));
    }

    public function edit($id)
    {
        $googleSheet = GoogleSheet::findOrFail($id);
        return view('googleSheets.edit', compact('googleSheet'));
    }

    public function update(Request $request, $id)
    {
        $googleSheet = GoogleSheet::findOrFail($id);
        $validated = $request->validate([
            'course_name' => 'required',
            'sheet_link' => 'required',
            'month' => 'required',
        ]);
        $googleSheet->update($validated);
        return redirect()->route('user.sheets.index')->with('success', 'Record updated successfully');
    }

    public function destroy($id)
    {
        $googleSheet = GoogleSheet::findOrFail($id);
        $googleSheet->delete();
        return redirect()->route('user.sheets.index')->with('success', 'Record deleted successfully');
    }
}
