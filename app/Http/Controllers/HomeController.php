<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    // Testing QR Code Generation
    public function qrcode()
    {
        $invoice = Invoice::find(1);
        $data = [
            'Supplier GSTIN'  => $invoice->tax_number,
            'Recepient GSTIN' => 310296660800003,
            'Document Number' => $invoice->id,
            'Document Type' => 'Invoice',
            'Document Date' => date('d-m-Y', strtotime($invoice->invoice_date)),
            'Number of Items' => $invoice->invoiceItems->count(),
            'Total Invoice Value' =>  'AED ' . number_format($invoice->total_amount, 2),
        ];

        // Format the data into a simple text format
        $textData = "Supplier GSTIN: " . $data['Supplier GSTIN'] . "\n" .
            "Recepient GSTIN: " . $data['Recepient GSTIN'] . "\n" .
            "Document Number: " . $data['Document Number'] . "\n" .
            "Document Type: " . $data['Document Type'] . "\n" .
            "Document Date: " . $data['Document Date'] . "\n" .
            "Number of Items: " . $data['Number of Items'] . "\n" .
            "Total Invoice Value: " . $data['Total Invoice Value'];

        // Generate a QR code using the data
        return QrCode::size(250)->generate($textData);
    }
}
