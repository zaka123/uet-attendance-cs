<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Quotation;
use App\Models\Room;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == 'user') {
            return redirect()->route('user.dashboard');
        }

        // get all instructors
        $total_instructors = User::where('role', 'user')->count();

        // get last 4 instructors
        $instructors = User::where('role', 'user')->latest()->take(4)->get();

        // return view
        return view('index', compact(
            'instructors',
            'total_instructors',
        ));
    }

    public function user()
    {
        return redirect()->route('user.sheets.index');
    }
}
