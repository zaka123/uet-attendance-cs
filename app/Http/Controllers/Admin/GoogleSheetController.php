<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GoogleSheet;
use Illuminate\Http\Request;

class GoogleSheetController extends Controller
{
    public function index()
    {
        $googleSheets = GoogleSheet::orderBy('id', 'desc')->paginate(5);
        return view('admin.googleSheets.index', compact('googleSheets'));
    }

    public function create()
    {
        abort(404);


        return view('admin.googleSheets.create');
    }

    public function store(Request $request)
    {
        abort(404);


        $validated = $request->validate([
            'course_name' => 'required',
            'sheet_link' => 'required',
            'month' => 'required',
        ]);

        $validated['user_id'] = auth()->user()->id;
        GoogleSheet::create($validated);
        // return back()->with('error', 'Failed to save record');
        return redirect()->route('sheets.index')->with('success', 'Record saved successfully');
    }

    public function show($id)
    {
        $googleSheet = GoogleSheet::findOrFail($id);
        return view('admin.googleSheets.show', compact('googleSheet'));
    }

    public function edit($id)
    {
        abort(404);


        $googleSheet = GoogleSheet::findOrFail($id);
        return view('googleSheets.edit', compact('googleSheet'));
    }

    public function update(Request $request, $id)
    {
        abort(404);


        $googleSheet = GoogleSheet::findOrFail($id);
        $validated = $request->validate([
            'course_name' => 'required',
            'sheet_link' => 'required',
            'month' => 'required',
        ]);
        $googleSheet->update($validated);
        return redirect()->route('sheets.index')->with('success', 'Record updated successfully');
    }

    public function destroy($id)
    {
        abort(404);

        
        $googleSheet = GoogleSheet::findOrFail($id);
        $googleSheet->delete();
        return redirect()->route('user.sheets.index')->with('success', 'Record deleted successfully');
    }
}
