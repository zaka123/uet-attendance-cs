<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $students = Student::orderBy('id', 'desc')->paginate(5);
        return view('students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'nullable',
            'emergency_1' => 'required',
            'emergency_2' => 'nullable',
            'cnic' => 'nullable',
            'joining_date' => 'nullable',
            'leaving_date' => 'nullable',
            'status' => 'nullable',
        ]);

        $student = Student::create($validated);
        // return back()->with('error', 'Failed to save student');
        return redirect()->route('students.index')->with('success', 'Student created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        return view('students.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Student $student)
    {
        $validated = $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'room_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'nullable',
            'emergency_1' => 'required',
            'emergency_2' => 'nullable',
            'cnic' => 'nullable',
            'joining_date' => 'nullable',
            'leaving_date' => 'nullable',
            'status' => 'nullable',

        ], [
            'room_id.required' => 'Please select a room',
        ]);

        $oldRoomId = $student->room_id;
        $updated = $student->update($validated);
        // dd($student);
        if ($updated) {
            return redirect()->route('students.index')->with('success', 'Student updated successfully');
        }
        return back()->with('error', 'Failed to update student');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {

        $deleted = $student->delete();
        if ($deleted) {
            return redirect()->route('students.index')->with('success', 'Student deleted successfully');
        }
        return back()->with('error', 'Failed to delete student');
    }
}
