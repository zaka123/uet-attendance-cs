<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function edit()
    {
        if(auth()->user()->role == 'user') {
            return view('auth.passwords.change-password');
        }
        return view('auth.passwords.change');
    }

    public function update(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'unique:users,email,'. $request->user()->id],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $request->user()->fill([
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ])->save();

        return redirect()->route('dashboard')->with('success', 'Password updated successfully.');
    }
}
