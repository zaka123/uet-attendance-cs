<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        // Get the search value from the request
        $search = $request->input('query');
        // if the search value is empty
        if (empty($search) || $search == '') {
            return redirect()->back();
        }
        // Search in the Customer name, Email, Invoice id and Status
        $invoices = Invoice::where('customer_name', 'like', '%' . $search . '%')
        ->orWhere('phone', 'like', '%' . $search . '%')
        ->orWhere('invoice_no', 'like', '%' . $search . '%')
        ->orWhere('status', 'like', '%' . $search . '%')
        ->orWhere('tax_number', 'like', '%' . $search . '%')
        ->paginate(7);

        // Return the search view with the resluts of the search
        return view('invoices.index', compact('invoices', 'search'));
    }
}
