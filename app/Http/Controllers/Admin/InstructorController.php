<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $instructors = User::orderBy('id', 'desc')->where('role', 'user')->paginate(5);
        return view('instructors.index', compact('instructors'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('instructors.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
            'role' => 'user',
        ]);
        // return back()->with('error', 'Failed to save student');
        return redirect()->route('instructors.index')->with('success', 'Student created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $student = User::find($id);
        return view('instructors.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $instructor)
    {
        return view('instructors.edit', compact('instructor'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $instructor)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'nullable',
        ]);

        $instructor->name = $validated['name'];
        $instructor->email = $validated['email'];
        if($request->password != null && $request->password != ""){
            $instructor->password = bcrypt($request->password);
        }
        $updated = $instructor->save();

        if ($updated) {
            return redirect()->route('instructors.index')->with('success', 'Record updated successfully');
        }
        return back()->with('error', 'Failed to update record');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $instructor = User::findOrFail($id);
        $deleted = $instructor->delete();
        if ($deleted) {
            return redirect()->route('instructors.index')->with('success', 'Record deleted successfully');
        }
        return back()->with('error', 'Failed to delete record');
    }
}
