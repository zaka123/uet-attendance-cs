<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\PasswordController;
use App\Http\Controllers\Admin\QuotationsController;
use App\Http\Controllers\Admin\SearchController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Instructor\GoogleSheetController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


/* Authentication Routes... */

Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware('auth')->group(function () {

    /* Dashboard Route */
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    // Change password routes...
    Route::get('/change/password', [PasswordController::class, 'edit'])->name('password.change');
    Route::post('/change/password', [PasswordController::class, 'update'])->name('password.change.post');
});

Route::group(['middleware' => ['auth', 'preventBackHistory', 'admin']], function () {

    /* Dashboard Route */


    /* Student Routes... */
    Route::resource('students', App\Http\Controllers\Admin\StudentController::class);
    Route::resource('instructors', App\Http\Controllers\Admin\InstructorController::class);
    Route::resource('sheets', App\Http\Controllers\Admin\GoogleSheetController::class);

    /* Search Route... */
    Route::get('search', [SearchController::class, 'search'])->name('search');
});

/*  Common Auth Routes... */
Route::prefix('user')->as('user.')->middleware(['auth', 'preventBackHistory', 'user'])->group(function () {

    Route::get('/dashboard', [DashboardController::class, 'user'])->name('dashboard');

    Route::resource('sheets', GoogleSheetController::class);

    /*  Change Password Routes... */
    // Route::get('/profile/change-password', [App\Http\Controllers\Auth\ChangePasswordController::class, 'index'])->name('password.change');
    // Route::put('/profile/update-password', [App\Http\Controllers\Auth\ChangePasswordController::class, 'update'])->name('password.update');

});

// Auth::routes();
